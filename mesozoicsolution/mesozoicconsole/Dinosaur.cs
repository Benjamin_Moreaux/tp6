using System;

namespace Mesozoic
{
    public class Dinosaur
    {
        protected string name;
        protected static string specie;
        protected int age;

        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            Dinosaur.specie = specie;
            this.age = age;
        }

        public string SayHello()
        {
            return String.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, Dinosaur.specie, this.age);
        }

        public string Roar()
        {
            return "Grrr";
        }


        public string Hug(Dinosaur dinosaur)
        {
            if (dinosaur == this)
            {
                return String.Format("Je suis {0} et je ne peux pas me faire de câlin à moi-même :'(.", this.name);
            }
            return String.Format("Je suis {0} et je fais un câlin à {1}.", this.name, dinosaur.GetName());
        }


        public string GetName()
        {
            return this.name;
        }

        public string GetSpecie()
        {
            return Dinosaur.specie;
        }

        public int GetAge()
        {
            return this.age;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public void SetSpecie(string specie)
        {
            Dinosaur.specie = specie;
        }

        public void SetAge(int age)
        {
            this.age = age;
        }
    }
}