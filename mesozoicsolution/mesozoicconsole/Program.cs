﻿using System;
using Mesozoic;

namespace mesozoicconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello from Mesozoic!");
            Dinosaur louis, nessie;

            nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Console.WriteLine("Présentation de nos dinosaures:");

            Dinosaur henry = new Dinosaur("Henry", 11, "Diplodocus");
            Console.WriteLine(henry.SayHello());
            Dinosaur louis = new Dinosaur("Louis", 12, "Stegausaurus");
            Console.WriteLine(louis.SayHello());
            Console.WriteLine(henry.SayHello());

            Console.WriteLine(louis.SayHello());
            Console.WriteLine(nessie.Roar());

            Console.WriteLine(louis.Hug(nessie));
            Console.WriteLine(nessie.SayHello());

            Console.WriteLine(louis.Hug(louis));
            Console.WriteLine(nessie.Hug(louis));

            Console.WriteLine("\nCréation d'un troupeau");

            Horde horde = new Horde();
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);

            Console.WriteLine(horde.IntroduceAll());
        }
    }
}
